from sys import argv as application_arguments
import re
from decimal import Decimal
from decimal import getcontext as decimal_get_context
import io
from typing import Tuple, Dict, List

# set global precision
decimal_get_context().prec = 200

MAX_LEN_ENCODABLE_STRING = 100
SUPPORTED_CHARS = " abcdefghijklmnopqrstuvwxyz"


def assert_encoded_binary_within_interval(encoded_binary_string, interval_bottom, interval_top):
    binary_value = binStringToDecimalFraction(encoded_binary_string)

    if not(binary_value >= interval_bottom and binary_value < interval_top):
        raise ValueError(
            "Encoded binary string's value does not fall between calculated interval")


def interval_from_decoded_string(decoded_str: str, alphabet) -> Tuple[Decimal, Decimal]:
    # arithmic encoding to determine the interval in which the encoded string will lie
    alphabet_intervals, _ = getAlphabetFractions(alphabet)
    low = Decimal(0.0)
    high = Decimal(1.0)
    encode_range = Decimal(1.0)

    # The assignment assumes we know the amount of characters which we're working with at all times.
    # Due to this no terminator symbol is used.
    for char in decoded_str:
        orig_low = low
        low = orig_low + (encode_range * alphabet_intervals[char]["bottom"])
        high = orig_low + (encode_range * alphabet_intervals[char]["top"])
        encode_range = high - low

    return (low, high)


def validate_decoded_str(decoded_str: str):
    if len(decoded_str) > MAX_LEN_ENCODABLE_STRING:
        raise ValueError(
            f"The length of the decoded string ({len(decoded_str)}) exceeds the maximum ({MAX_LEN_ENCODABLE_STRING}).")

    for char in decoded_str:
        if char not in SUPPORTED_CHARS:
            raise ValueError("Decoded string contains invalid chars")


def read_decoded(file: io.TextIOWrapper) -> str:
    # Read out the file, and replace newline characters.
    return file.read().replace("\n", "").replace("\r", "")


def build_alphabet(decoded_str: str):
    from typing import Dict

    dictionary: Dict[str, int] = {}

    for char in decoded_str:
        if char in dictionary:
            dictionary[char] += 1
        else:
            dictionary[char] = 1

    alphabet = []
    for char in SUPPORTED_CHARS:
        if char in dictionary:
            alphabet.append([char, dictionary[char]])
    return alphabet


def test_mode(args):
    input_file = args.decoded_file
    original_decoded_string = read_decoded(input_file)

    interval_bottom, interval_top, alphabet = encodeToRange(
        original_decoded_string)
    encoded_string = (interval_bottom+interval_top) / Decimal(2)

    decoded_string = decodeDecimalFraction(encoded_string, Alphabet(alphabet))

    if original_decoded_string == decoded_string:
        print("Encode/decode successfull!")
    else:
        raise ValueError(
            "Message was not the same after encode/decode process")


def encode_mode(args):
    decoded_string = read_decoded(args.decoded_file)
    interval_bottom, interval_top, alphabet = encodeToRange(decoded_string)
    encoded_binary_string = generateBinaryStringInRange(
        interval_bottom, interval_top)

    # assert_encoded_binary_within_interval(
    #     encoded_binary_string, interval_bottom, interval_top)

    from json import dumps
    args.encoded_file.write(encoded_binary_string)
    args.alphabet_file.write(dumps(alphabet))


def decode_mode(args):
    from json import load
    encoded = args.encoded_file.read()
    cursor = binStringToDecimalFraction(encoded)
    alphabet = Alphabet(load(args.alphabet_file))

    decoded_message = decodeDecimalFraction(cursor, alphabet)
    args.decoded_file.write(decoded_message)


class Alphabet:
    # the start of intervals and their corresponding strings
    values: List[Tuple[Decimal, str]] = []
    symbol_count: int = 0

    def __init__(self, alphabet_info: List[Tuple[str, int]]):
        self.symbol_count = sum(map(lambda i: i[1], alphabet_info))
        symbol_count_decimal = Decimal(self.symbol_count)

        cursor = Decimal(0)
        for [ch, count] in alphabet_info:
            self.values.append((cursor, ch))
            cursor += Decimal(count) / symbol_count_decimal

    def str_at(self, at: Decimal) -> str:
        """
        Returns the string of the interval 'at' falls within.
        """
        _, _, s = self.at(at)
        return s

    def at(self, at: Decimal) -> Tuple[Decimal, Decimal, str]:
        """
        Returns a tuple of the start, end and str of the interval 'at' falls within.
        """

        # linear search because cba
        current_str = ""
        current_start = Decimal(0)
        current_end = Decimal(1)
        for next_start, next_str in self.values:
            if at < next_start:
                current_end = next_start
                break
            current_start = next_start
            current_str = next_str
        return (current_start, current_end, current_str)


def getAlphabetFractions(alphabetInfo: list) -> Tuple[Dict[str, Dict[str, Decimal]], Decimal]:
    intervals = {}
    progress = Decimal(0)
    symbol_count = Decimal(sum(map(lambda tuple: tuple[1], alphabetInfo)))

    for char, count in alphabetInfo:
        top = progress + (Decimal(count) / symbol_count)
        intervals[char] = {"bottom": progress, "top": top}
        progress = top

    return (intervals, symbol_count)


def binStringToDecimalFraction(binStr: str) -> Decimal:
    bits = map(lambda ch: ch == "1", binStr)

    value = Decimal(0)
    step = Decimal(0.5)

    for high in bits:
        if high:
            value += step
        step /= 2

    return value


def encodeToRange(text: str) -> Tuple[Decimal, Decimal, List[List]]:
    validate_decoded_str(text)
    alphabet = build_alphabet(text)
    interval_bottom, interval_top = interval_from_decoded_string(
        text, alphabet)

    return (interval_bottom, interval_top, alphabet)


def decodeDecimalFraction(cursor: Decimal, alphabet: Alphabet) -> str:
    decoded_message = ""
    for _ in range(alphabet.symbol_count):
        begin, end, s = alphabet.at(cursor)
        decoded_message += s
        cursor = (cursor - begin) * (1 / (end - begin))

    return decoded_message


def generateBinaryStringInRange(bottom, top):
    binary = ""

    cursor = Decimal(0)
    step = Decimal(0.5)

    while True:
        if cursor + step < top:
            binary += "1"
            cursor += step
        else:
            binary += "0"
        step /= 2

        if bottom <= cursor < top:
            return binary


def main():
    import argparse
    parser = argparse.ArgumentParser(description="Arithmetic Encoder/Decoder")

    subparsers = parser.add_subparsers(
        dest="mode", required=True, title="modes")

    test_parser = subparsers.add_parser(
        "test",
        help="test wether a file is encodable",
        description="test whether a file is encodable"
    )
    test_parser.add_argument(
        "decoded_file",
        type=argparse.FileType('rt'),
        help="the file containing a decoded string to test"
    )
    test_parser.set_defaults(run_mode=test_mode)

    encode_parser = subparsers.add_parser(
        "encode",
        help="encodes a file",
        description="encodes a file"
    )
    encode_parser.add_argument(
        "decoded_file",
        type=argparse.FileType('rt'),
        help="the file containing the string to encode"
    )
    encode_parser.add_argument(
        "encoded_file",
        type=argparse.FileType('wt'),
        help="the file to save the encoded string in"
    )
    encode_parser.add_argument(
        "alphabet_file",
        type=argparse.FileType('wt'),
        help="the file to save the alphabet information in"
    )
    encode_parser.set_defaults(run_mode=encode_mode)

    decode_parser = subparsers.add_parser(
        "decode",
        help="decodes a file",
        description="decodes a file"
    )
    decode_parser.add_argument(
        "encoded_file",
        type=argparse.FileType('rt'),
        help="the file containing the encoded string to decode"
    )
    decode_parser.add_argument(
        "alphabet_file",
        type=argparse.FileType('rt'),
        help="the file containing the alphabet information of the encoded string"
    )
    decode_parser.add_argument(
        "decoded_file",
        type=argparse.FileType('wt'),
        help="the file to save to decoded string in"
    )
    decode_parser.set_defaults(run_mode=decode_mode)

    args = parser.parse_args()
    args.run_mode(args)


main()
