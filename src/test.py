from os.path import join, dirname, realpath
from queue import Queue
import threading

CODEC_FILENAME = join(dirname(realpath(__file__)), "codec.py")

FILENAME_INPUT: str = "input"
FILENAME_DECODED: str = "decoded"
FILENAME_ENCODED: str = "encoded"
FILENAME_ALPHABET: str = "alphabet"

VALID_CHARACTERS: str = " abcdefghijklmnopqrstuvwxyz"

ROUNDS = 1000
MAX_LENGTH = 100
TIMEOUT = 1
THREAD_COUNT = 4

work_left = ROUNDS


def test_for_string(string: str) -> bool:
    from tempfile import TemporaryDirectory
    from subprocess import run

    with TemporaryDirectory() as temp_dirname:
        input_f = join(temp_dirname, FILENAME_INPUT)
        decoded_f = join(temp_dirname, FILENAME_DECODED)
        encoded_f = join(temp_dirname, FILENAME_ENCODED)
        alphabet_f = join(temp_dirname, FILENAME_ALPHABET)

        with open(input_f, "wt") as handle:
            handle.write(string)

        try:
            run(
                ["python", CODEC_FILENAME, "encode", input_f, encoded_f, alphabet_f],
                check=True,
                timeout=TIMEOUT
            )
            run(
                ["python", CODEC_FILENAME, "decode", encoded_f, alphabet_f, decoded_f],
                check=True,
                timeout=TIMEOUT
            )
        except Exception:
            return False

        with open(decoded_f, "rt") as handle:
            return handle.read() == string


def work(result_queue: Queue, mutex: threading.Lock):
    while True:
        with mutex:
            global work_left
            if work_left > 0:
                work_left -= 1
            else:
                break
        string = random_string()
        if(not test_for_string(string)):
            result_queue.put(string)


def random_string() -> str:
    import random
    length = random.randrange(MAX_LENGTH + 1)
    chars = map(lambda _: random.choice(VALID_CHARACTERS), range(length))
    return "".join(chars)


def main():
    from sys import stdout
    from math import floor
    from time import sleep

    result_queue = Queue(THREAD_COUNT * 8)
    mutex = threading.Lock()

    threads = []
    for index in range(THREAD_COUNT):
        thread = threading.Thread(target=work, args=(result_queue, mutex))
        thread.start()
        threads.append(thread)

    while True:
        with mutex:
            global work_left
            current_work_left = work_left

        while not result_queue.empty():
            stdout.write(f"\x1b[2K{result_queue.get()}\n")
            pass

        r = ROUNDS - work_left
        progress = r / ROUNDS
        bars = round(40 * progress)
        percent = floor(progress * 100)
        round_str = str(r).rjust(len(str(ROUNDS)))
        stdout.write(f"{round_str}/{ROUNDS} [{'#' * bars + '-' * (40 - bars)}] {percent: 3}%\r")
        stdout.flush()

        if current_work_left == 0:
            break

        sleep(0)

    print("")

    for thread in threads:
        thread.join()


main()
